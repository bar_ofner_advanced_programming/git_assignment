#ifndef COURSE_H
#define COURSE_H
#include <string>
using namespace std;
//Representation of a Course grade-book
//test - 25%
//final exam - 50%

//can only change grades after creation.
class Course
{
public:
	//Methods
	void init(string name, int test1, int test2, int exam);
	int* getGradesList();
	string getName();
	double getFinalGrade();


private: //what fields do you need?
	string courseName;
	int grades[3]; // two first are the tests and the last is the exam
};

#endif