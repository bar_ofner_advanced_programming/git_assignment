#include "student.h"

//initializes the student class
void Student::init(string name, Course** courses, int crsCount){
	studentName = name;
	courseArr = courses;
	courseCount = crsCount;
}

//setter function for the name of the student
void Student::setName(string name){
	studentName = name;
}

//getter function for the name of the student
string Student::getName(){
	return studentName;
}

//getter function for the course cpunt of the student
int Student::getCrsCount(){
	return courseCount;
}

//getter function for the courses array of the student
Course** Student::getCourses(){
	return courseArr;
}

//gets the average value of the student's grades
double Student::getAvg(){
	int i;
	double sum = 0;
	for (i = 0; i < courseCount; i++){
		sum += (courseArr[i])->getFinalGrade();
	}
	return (sum / courseCount);
}