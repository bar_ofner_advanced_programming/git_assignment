#include "Course.h"
#include <iostream>

//initializes the course class
void Course::init(std::string name, int test1, int test2, int exam){
	courseName = name;
	grades[0] = test1;
	grades[1] = test2;
	grades[2] = exam;
}

//getter function for the grades list
int* Course::getGradesList(){
	return grades;
}

//getter function for the name of the course
string Course::getName(){
	return courseName;
}

//getter function for the final grade
double Course::getFinalGrade(){
	return ((grades[0] / 4) + (grades[1] / 4) + (grades[2] / 2));
}